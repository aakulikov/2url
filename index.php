<?php

// Initialize the session
session_start();

// Check if the user is logged in, if not then redirect him to login page
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
    header('location: http://' . $_SERVER['HTTP_HOST'] . '/auth/login.php');
    exit;
}
require_once 'table_service.php';
?>
<!DOCTYPE html>
<html>
<head>
    <title>HOME | 2url</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css">
    <meta charset="utf-8">
</head>
<style>
    html{
        height: 100%;
        background: aliceblue;
    }
    body{
        padding-top:0;
        background: #f4f4f4;
        height: 100%;
        width:80%;
        margin:0 auto;
    }
</style>
<body>
<span style="position: absolute; right:0; top:0; background: antiquewhite;"><a href="./auth/logout.php">logout</a></span>
<h1>Сокращатель ссылок</h1>
<form method="post" action="worker.php">
    <input name="url" type="url" placeholder="Введите url...">
    <input type="submit">
</form>

<div class="row">
    <div class="col">
        <?php if ($deleteMsg != NULL){echo $deleteMsg;} ?>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead><tr>

                    <th>№</th>
                    <th>Длинная</th>
                    <th>Короткая</th>
                    <th>Операции</th>
                </thead>
                <tbody>
                <?php
                if(is_array($fetchData)){
                    $sn=1;
                    foreach($fetchData as $data){ //var_dump($data);
                        ?>
                        <tr data-id="<?php echo $data['id']; ?>">
                            <td class="n"> <?php echo $sn; ?> </td>
                            <td class="long"><a href="<?php if ($data['longUrl']){echo $data['longUrl']; }; ?>"><?php if ($data['longUrl']){echo $data['longUrl']; }; ?></a></td>
                            <td class="short"><a href="http://2url.loc/<?php if ($data['shortUrl']){echo $data['shortUrl']; }; ?>">http://2url.loc/<?php if ($data['shortUrl']){echo $data['shortUrl']; }; ?></a></td>
                            <td class="operations"><a class="delete" href="#">Удалить</a></td>
                        </tr>
                        <?php
                        $sn++;}}else{ ?>
                <tr>
                    <td colspan="2">
                        <?php echo $fetchData; ?>
                    </td>
                <tr>
                    <?php
                    }?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!--script>
    document.addEventListener('DOMContentLoaded',ready)
    function ready(){
        document.querySelectorAll('.delete').forEach((elem)=>{
            let id = elem.parentNode.parentNode.dataset.id
            elem.addEventListener('click', deleteAjax(id))
        })
    }
    function deleteAjax(id) {
        var xhr=new XMLHttpRequest();
        xhr.onreadystatechange=function() {
            if (this.readyState==4 && this.status==200) {
                document.getElementById("livesearch").innerHTML=this.responseText;
                document.getElementById("livesearch").style.border="1px solid #A5ACB2";
            }
        }
        xhr.open("GET","livesearch.php?q="+str,true);
        xhr.send();
    }
</script-->
</body>
</html>
<?php
