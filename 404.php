<?php

namespace Shortener;

$req = substr($_SERVER['REQUEST_URI'], 1);
require_once 'Shortener\ShortenerService.php';
if (strlen($req) === ShortenerService::SHORT_URL_LENGTH) {

    require_once 'Shortener\ShortenerDatabaseService.php';

    require 'Shortener/db.php';
    require_once 'RedisHelper/redis.php';

    http_response_code(302);

    $data = $redis->get($req);

    if (!$data){
        $db = new ShortenerDatabaseService();
        $app = new ShortenerService($db);
        $forward = $app->getLongUrl($req);
        $forward = $forward.'?powered=byredis'; //TODO: DEV ONLY
        $redis->set($req, $forward, 15);
    } else {
        $forward = $data;
    }

    header("location: ".$forward);
}
else{
    http_response_code(404);
    var_dump($_SERVER['REQUEST_URI']);
    echo 'and htmlspec:';
    var_dump(htmlspecialchars($_SERVER['REQUEST_URI']));
    die('Sorry. Page do not exist');
}
