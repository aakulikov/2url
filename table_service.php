<?php
// Initialize the session
session_start();

// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header('location: http://'.$_SERVER['HTTP_HOST'].'/auth/login.php');
    exit;
}
require("Shortener/db.php");
$db = $conn;

$fetchData = fetch_data($db, 'urls');

function fetch_data($db, $table){
//    if(empty($db)){
//        $msg= "Database connection error";
//    }elseif (empty($columns) || !is_array($columns)) {
//        $msg="columns Name must be defined in an indexed array";
//    }elseif(empty($tableName)){
//        $msg= "Table Name is empty";
//    }else{


    //$columnName = implode(", ", $columns);
    if (htmlspecialchars($_SESSION["role"]) === "administrator"){
        $query = "select * FROM ".$table;
    } else {
        $query = "select * FROM $table WHERE `user` = ".htmlspecialchars($_SESSION['username']);
    }
    $result = $db->query($query);
    $msg = $result;

    if($result == true){
        if ($result->num_rows > 0) {
            $row= mysqli_fetch_all($result, MYSQLI_ASSOC);
            $msg= $row;
        } else {
            $msg= "Нет ссылок";
        }
    }else{
        $msg= mysqli_error($db);
    }
    //}
    return $msg;
}
?>