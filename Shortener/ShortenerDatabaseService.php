<?php

declare(strict_types=1);

namespace Shortener;
use mysqli;
//use \PDO;
//require 'db.php';
require_once 'Shortener/ShortenerPersistenceInterface.php';
/**
 * This class provides a set of methods for implementing a small URL shortener
 * service based on the TableGateway pattern.
 */
final class ShortenerDatabaseService implements ShortenerPersistenceInterface
{
    private $db;
    public function __construct()
    {
        $this->db = new mysqli('localhost', 'root', '', 'shortener');
    }

    /**
     * Retrieves the un-shortened URL, based on the shortened URL provided.
     */
    public function getLongUrl(string $shortUrl): string
    {
        $query = "SELECT longUrl FROM urls WHERE shortUrl = '".$shortUrl."'";
        $result = $this->db->query($query);
        if ($result->num_rows > 0) {
            // выводим результаты
            while($row = $result->fetch_assoc()) {
                $longUrl = $row["longUrl"];
                echo "Long URL: " . $longUrl;
            }
        } else {
            echo "No results found.";
        }
        return $longUrl;
    }

    public function hasShortUrl(string $shortUrl): bool
    {
        $query = "SELECT COUNT(*) FROM urls WHERE shortUrl = '".$shortUrl."'";
        $result = $this->db->query($query);

        return $result;
    }

    public function persistUrl(string $longUrl, string $shortenedUrl, $username=NULL): bool
    {
        $query = "INSERT INTO urls (`longUrl`, `shortUrl`, `user`) VALUES ('".$longUrl."','".$shortenedUrl."', '".$username."')";
        $this->db->query($query);
        return true;
    }
    public function __destruct()
    {
        $this->db->close();
    }

}