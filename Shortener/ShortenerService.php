<?php

declare(strict_types=1);

namespace Shortener;
//require_once 'Shortener/ShortenerPersistenceInterface.php';
final class ShortenerService
{
    public const SHORT_URL_LENGTH = 9;
    public const RANDOM_BYTES = 32;

    private ShortenerPersistenceInterface $shortenerPersistence;

    public function __construct(
        ShortenerPersistenceInterface $shortenerPersistence
    ) {
        $this->shortenerPersistence = $shortenerPersistence;
    }

    public function getShortUrl(string $longUrl): string
    {
        $shortUrl = $this->shortenUrl($longUrl);
        $this
            ->shortenerPersistence
            ->persistUrl($longUrl, $shortUrl);

        return $shortUrl;
    }

    public function hasShortUrl(string $longUrl): bool
    {
        return $this->shortenerPersistence->hasShortUrl($longUrl);
    }

    public function getLongUrl(string $shortUrl): string
    {
        $longUrl = $this
            ->shortenerPersistence
            ->getLongUrl($shortUrl);

        return $longUrl;
    }

    protected function shortenUrl(string $longUrl): string
    {
        $shortenedUrl = substr(
            base64_encode(
                sha1(
                    uniqid(
                        random_bytes(self::RANDOM_BYTES),
                        true
                    )
                )
            ),
            0,
            self::SHORT_URL_LENGTH
        );

        return $shortenedUrl;
    }
    public function getShortenUrl(string $longUrl): string
    {
        return $this->shortenUrl($longUrl);
    }
}