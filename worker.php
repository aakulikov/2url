<?php

namespace Shortener;

use mysql_xdevapi\Exception;


if (isset($_POST['url'])){

    require_once 'Shortener/ShortenerDatabaseService.php';
    require_once __DIR__.'/Shortener/ShortenerService.php';
    require_once 'Shortener/ShortenerPersistenceInterface.php';
    require_once 'RedisHelper/redis.php';

    $db = new ShortenerDatabaseService();
    $app = new ShortenerService($db);
    $shortedUrl = $app->getShortenUrl( htmlspecialchars($_POST['url']) );
    try{
        $db->persistUrl($_POST['url'], $shortedUrl, $_SESSION['username']);
        $redis->set($shortedUrl, $_POST['url'].'?powered=byredis', 15);
        echo 'Ссылка на '.$_POST['url'].' успешно создана <a href="http://2url.loc/'.$shortedUrl.'">http://2url.loc/'.$shortedUrl.'</a><br><a href="../">На главную</a>';
    } catch (Exception $exception){
        var_dump($exception);
        die('Что-то пошло не так :(');
    }

} else{
    header('location:index.php');
}